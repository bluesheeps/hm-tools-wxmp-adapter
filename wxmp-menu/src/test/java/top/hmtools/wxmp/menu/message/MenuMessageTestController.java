package top.hmtools.wxmp.menu.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import top.hmtools.wxmp.core.annotation.WxmpController;
import top.hmtools.wxmp.core.annotation.WxmpRequestMapping;
import top.hmtools.wxmp.menu.models.eventMessage.ClickEventMessage;
import top.hmtools.wxmp.menu.models.eventMessage.LocationSelectEventMessage;
import top.hmtools.wxmp.menu.models.eventMessage.PicPhotoOrAlbumEventMessage;
import top.hmtools.wxmp.menu.models.eventMessage.PicSysPhotoEventMessage;
import top.hmtools.wxmp.menu.models.eventMessage.PicWeixinEventMessage;
import top.hmtools.wxmp.menu.models.eventMessage.ScancodePushEventMessage;
import top.hmtools.wxmp.menu.models.eventMessage.ViewEventMessage;
import top.hmtools.wxmp.menu.models.eventMessage.ViewMiniprogramEventMessage;

@WxmpController
public class MenuMessageTestController {

	final Logger logger = LoggerFactory.getLogger(MenuMessageTestController.class);
	private ObjectMapper objectMapper;
	
	/**
	 * 点击菜单拉取消息时的事件推送
	 * @param msg
	 */
	@WxmpRequestMapping
	public void ClickEventMessage(ClickEventMessage msg){
		this.printFormatedJson("点击菜单拉取消息时的事件推送", msg);
	}
	
	/**
	 * 点击菜单跳转链接时的事件推送
	 * @param msg
	 */
	@WxmpRequestMapping
	public void ViewEventMessage(ViewEventMessage msg){
		this.printFormatedJson("点击菜单跳转链接时的事件推送", msg);
	}
	
	/**
	 * scancode_push：扫码推事件的事件推送
	 * @param msg
	 */
	@WxmpRequestMapping
	public void ScancodePushEventMessage(ScancodePushEventMessage msg){
		this.printFormatedJson("scancode_push：扫码推事件的事件推送", msg);
	}
	
	/**
	 * pic_sysphoto：弹出系统拍照发图的事件推送
	 * @param msg
	 */
	@WxmpRequestMapping
	public void PicSysPhotoEventMessage(PicSysPhotoEventMessage msg){
		this.printFormatedJson("pic_sysphoto：弹出系统拍照发图的事件推送", msg);
	}
	
	/**
	 * pic_photo_or_album：弹出拍照或者相册发图的事件推送
	 * @param msg
	 */
	@WxmpRequestMapping
	public void PicPhotoOrAlbumEventMessage(PicPhotoOrAlbumEventMessage msg){
		this.printFormatedJson("pic_photo_or_album：弹出拍照或者相册发图的事件推送", msg);
	}
	
	/**
	 * pic_weixin：弹出微信相册发图器的事件推送
	 * @param msg
	 */
	@WxmpRequestMapping
	public void PicWeixinEventMessage(PicWeixinEventMessage msg){
		this.printFormatedJson("pic_weixin：弹出微信相册发图器的事件推送", msg);
	}
	
	/**
	 * location_select：弹出地理位置选择器的事件推送
	 * @param msg
	 */
	@WxmpRequestMapping
	public void LocationSelectEventMessage(LocationSelectEventMessage msg){
		this.printFormatedJson("location_select：弹出地理位置选择器的事件推送", msg);
	}
	
	/**
	 * 点击菜单跳转小程序的事件推送
	 * @param msg
	 */
	@WxmpRequestMapping
	public void ViewMiniprogramEventMessage(ViewMiniprogramEventMessage msg){
		this.printFormatedJson("点击菜单跳转小程序的事件推送", msg);
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * 格式化打印json字符串到控制台
	 * @param title
	 * @param obj
	 */
	protected synchronized void printFormatedJson(String title,Object obj) {
		if(this.objectMapper == null){
			this.objectMapper = new ObjectMapper();
		}
		try {
			//阿里的fastjson具有很好的兼容性，所以才多次一举
			String jsonString = JSON.toJSONString(obj);
			Object tempObj = JSON.parse(jsonString);
			String formatedJsonStr = this.objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tempObj);
			this.logger.info("\n{}：\n{}",title,formatedJsonStr);
		} catch (JsonProcessingException e) {
			this.logger.error("格式化打印json异常：",e);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
