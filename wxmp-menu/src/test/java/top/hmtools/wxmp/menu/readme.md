#### 说明
1. 本包是微信公众号目录接口操作工具包单元测试示例。
2. `top.hmtools.wxmp.menu.apis.IMenuApiTest`基本菜单操作接口测试
3. `top.hmtools.wxmp.menu.message.WxmpMessageTest` 菜单事件模拟接收、解析、处理测试
4. `top.hmtools.wxmp.menu.apis.IConditionalMenuApiTest`个性化菜单接口
