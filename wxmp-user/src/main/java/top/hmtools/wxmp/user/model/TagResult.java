package top.hmtools.wxmp.user.model;

public class TagResult {

	/*
	 * 标签id，由微信分配
	 */
	private long id;
	
	/**
	 * 标签名，UTF8编码
	 */
	private String name;
	
	private long count;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "TagResult [id=" + id + ", name=" + name + ", count=" + count + "]";
	}

	
}
