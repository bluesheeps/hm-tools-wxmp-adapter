package top.hmtools.wxmp.user.apis;

import org.junit.Test;

import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.user.BaseTest;
import top.hmtools.wxmp.user.model.RemarkParam;

public class IRemarkApiTest extends BaseTest {
	
	private IRemarkApi remarkApi;
	

	@Test
	public void testUpdateRemark() {
		RemarkParam remarkParam = new RemarkParam();
		remarkParam.setOpenid("o-OZ0v8HcoPJiNlZLTOZYrkeZUG0");
		remarkParam.setRemark("wahahahaahaah");
		ErrcodeBean updateRemark = this.remarkApi.updateRemark(remarkParam);
		this.printFormatedJson("用户管理--设置用户备注名	", updateRemark);
	}


	@Override
	public void initSub() {
		this.remarkApi = this.wxmpSession.getMapper(IRemarkApi.class);
	}

}
