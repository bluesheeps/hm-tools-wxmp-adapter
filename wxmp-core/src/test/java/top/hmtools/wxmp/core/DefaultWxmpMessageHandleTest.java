package top.hmtools.wxmp.core;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import top.hmtools.wxmp.core.eventModels.TextMessage;

/**
 * 测试解析微信公众号xml消息
 * @author HyboWork
 *
 */
public class DefaultWxmpMessageHandleTest {
	
	private final Logger logger = LoggerFactory.getLogger(DefaultWxmpMessageHandleTest.class);
	
	private DefaultWxmpMessageHandle  defaultWxmpMessageHandle;
	
	private ObjectMapper objectMapper;

	/**
	 * 微信公众号xml消息解析准备工作
	 */
	@Test
	public void testAddMessageMetaInfo() {
		//实例化消息处理handle
		this.defaultWxmpMessageHandle = new DefaultWxmpMessageHandle();
		
		//实例化实际处理指定消息的controller，并加入handle映射
		WxmpControllerTest wxmpControllerTest = new WxmpControllerTest();
		this.defaultWxmpMessageHandle.addMessageMetaInfo(wxmpControllerTest);
		
		//打印映射信息
		this.printFormatedJson("事件消息映射", this.defaultWxmpMessageHandle.getEventMap());
		this.printFormatedJson("消息类型映射", this.defaultWxmpMessageHandle.getMsgTypeMap());
	}

	/**
	 * 测试委托解析处理微信公众号xml消息
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	@Test
	public void testProcessXmlData() throws InstantiationException, IllegalAccessException {
		this.testAddMessageMetaInfo();
		//验证基础消息
		String xml = "<xml><ToUserName><![CDATA[toUser]]></ToUserName><FromUserName><![CDATA[fromUser]]></FromUserName><CreateTime>1348831860</CreateTime><MsgType><![CDATA[text]]></MsgType><Content><![CDATA[this is a test]]></Content><MsgId>1234567890123456</MsgId></xml>";
		Object processXmlData = this.defaultWxmpMessageHandle.processXmlData(xml);
		this.printFormatedJson("解析TextMessage的微信公众号xml消息", processXmlData);
		
		//验证事件消息
		xml="<xml><ToUserName><![CDATA[toUser]]></ToUserName><FromUserName><![CDATA[fromUser]]></FromUserName><CreateTime>1442401093</CreateTime><MsgType><![CDATA[event]]></MsgType><Event><![CDATA[naming_verify_success]]></Event><ExpiredTime>1442401093</ExpiredTime></xml>";
		processXmlData = this.defaultWxmpMessageHandle.processXmlData(xml);
		this.printFormatedJson("验证事件消息", processXmlData);
		
		//验证暂未实现处理器的事件消息
		xml="<xml><ToUserName><![CDATA[toUser]]></ToUserName><FromUserName><![CDATA[FromUser]]></FromUserName><CreateTime>123456789</CreateTime><MsgType><![CDATA[event]]></MsgType><Event><![CDATA[CLICK]]></Event><EventKey><![CDATA[EVENTKEY]]></EventKey></xml>";
		processXmlData = this.defaultWxmpMessageHandle.processXmlData(xml);
		this.printFormatedJson("验证暂未实现处理器的事件消息", processXmlData);
		
		//验证不存在的xml消息
		xml="<xml></xml>";
		processXmlData = this.defaultWxmpMessageHandle.processXmlData(xml);
		this.printFormatedJson("验证不存在的xml消息",processXmlData);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * 格式化打印json字符串到控制台
	 * @param title
	 * @param obj
	 */
	protected synchronized void printFormatedJson(String title,Object obj) {
		if(this.objectMapper == null){
			this.objectMapper = new ObjectMapper();
		}
		try {
			//阿里的fastjson具有很好的兼容性，所以才多次一举
			String jsonString = JSON.toJSONString(obj);
			Object tempObj = JSON.parse(jsonString);
			String formatedJsonStr = this.objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tempObj);
			this.logger.info("\n{}：\n{}",title,formatedJsonStr);
		} catch (JsonProcessingException e) {
			this.logger.error("格式化打印json异常：",e);
		}
	}

}
