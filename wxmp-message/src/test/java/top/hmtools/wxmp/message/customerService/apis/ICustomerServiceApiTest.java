package top.hmtools.wxmp.message.customerService.apis;

import java.io.File;

import org.junit.Test;

import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.core.model.message.enums.MsgType;
import top.hmtools.wxmp.message.BaseTest;
import top.hmtools.wxmp.message.customerService.model.KfAccountListResult;
import top.hmtools.wxmp.message.customerService.model.KfAccountParam;
import top.hmtools.wxmp.message.customerService.model.TypingStatusParam;
import top.hmtools.wxmp.message.customerService.model.UploadHeadImgParam;
import top.hmtools.wxmp.message.customerService.model.sendMessage.TextBean;
import top.hmtools.wxmp.message.customerService.model.sendMessage.TextCustomerServiceMessage;

/**
 * 本旧版本客服功能已失效？？
 * <a href="https://mp.weixin.qq.com/cgi-bin/announce?action=getannouncement&key=1464266075&version=12&lang=zh_CN">微信公众号客服说明</a>
 * @author HyboWork
 *
 */
public class ICustomerServiceApiTest extends BaseTest{
	
	private ICustomerServiceApi customerServiceApi;

	/**
	 * 客服帐号管理--添加客服帐号  
	 * 
	 */
	@Test
	public void testAddKfAcount() {
		KfAccountParam kfAccountParam = new KfAccountParam();
		kfAccountParam.setKf_account("kefu001@hybo.net");
		kfAccountParam.setNickname("kefu001");
		kfAccountParam.setPassword("pwd1029384756");
		ErrcodeBean result = this.customerServiceApi.addKfAcount(kfAccountParam);
		this.printFormatedJson("客服帐号管理--添加客服帐号", result);
	}

	@Test
	public void testUpdateKfAcount() {
		KfAccountParam kfAccountParam = new KfAccountParam();
		kfAccountParam.setKf_account("kefu001@hybo.net");
		kfAccountParam.setNickname("kefu001");
		kfAccountParam.setPassword("pwd1029384756");
		ErrcodeBean result = this.customerServiceApi.updateKfAcount(kfAccountParam);
		this.printFormatedJson("客服帐号管理--修改客服帐号", result);
	}

	@Test
	public void testDeleteKfAcount() {
		KfAccountParam kfAccountParam = new KfAccountParam();
		kfAccountParam.setKf_account("kefu001@hybo.net");
		kfAccountParam.setNickname("kefu001");
		kfAccountParam.setPassword("pwd1029384756");
		ErrcodeBean result = this.customerServiceApi.deleteKfAcount(kfAccountParam);
		this.printFormatedJson("客服帐号管理--删除客服帐号", result);
	}

	@Test
	public void testUploadHeadImg() {
		UploadHeadImgParam uploadHeadImgParam = new UploadHeadImgParam();
		uploadHeadImgParam.setImage(new File("E:\\tmp\\aaaa.png"));
		uploadHeadImgParam.setKf_account("kefu001@hybo.net");
		ErrcodeBean result = this.customerServiceApi.uploadHeadImg(uploadHeadImgParam);
		this.printFormatedJson("客服帐号管理--上传客服头像", result);
	}

	@Test
	public void testGetKfList() {
		KfAccountListResult result = this.customerServiceApi.getKfList();
		this.printFormatedJson("客服帐号管理--1.5 获取所有客服账号", result);
	}

	/**
	 * 此接口可用，已正常发送消息出去成功
	 */
	@Test
	public void testSendMessage() {
		TextCustomerServiceMessage message = new TextCustomerServiceMessage();
		message.setMsgtype(MsgType.text);
		TextBean textBean = new TextBean();
		textBean.setContent("oooookkkkkkk");
		message.setText(textBean);
		message.setTouser("o-OZ0v8HcoPJiNlZLTOZYrkeZUG0");
		
		ErrcodeBean result = this.customerServiceApi.sendMessage(message);
		this.printFormatedJson("客服帐号管理--2 客服接口-发消息", result);
	}

	/**
	 * 此接口可用，已正常发送“输入中”状态到用户微信
	 */
	@Test
	public void testTypingStatus() {
		TypingStatusParam typingStatusParam = new TypingStatusParam();
		typingStatusParam.setCommand("Typing");
		typingStatusParam.setTouser("o-OZ0v8HcoPJiNlZLTOZYrkeZUG0");
		ErrcodeBean result = this.customerServiceApi.typingStatus(typingStatusParam );
		this.printFormatedJson("客服账号管理--3 客服接口-客服输入状态", result);
	}

	@Override
	public void initSub() {
		this.customerServiceApi = this.wxmpSession.getMapper(ICustomerServiceApi.class);
	}

}
