package top.hmtools.wxmp.message.group.model.preview;

/**
 * Auto-generated: 2019-08-27 18:20:24
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Wxcard {

	private Card_ext card_ext;
	private String card_id;

	public void setCard_ext(Card_ext card_ext) {
		this.card_ext = card_ext;
	}

	public Card_ext getCard_ext() {
		return card_ext;
	}

	public void setCard_id(String card_id) {
		this.card_id = card_id;
	}

	public String getCard_id() {
		return card_id;
	}

}