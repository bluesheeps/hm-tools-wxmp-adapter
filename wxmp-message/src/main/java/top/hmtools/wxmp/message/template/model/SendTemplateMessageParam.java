package top.hmtools.wxmp.message.template.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Auto-generated: 2019-08-29 11:48:4
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class SendTemplateMessageParam {

	private String touser;
	private String template_id;
	private String url;
	private Miniprogram miniprogram;
	private Map<String, DataItem> data;

	public String getTouser() {
		return touser;
	}

	public void setTouser(String touser) {
		this.touser = touser;
	}

	public String getTemplate_id() {
		return template_id;
	}

	public void setTemplate_id(String template_id) {
		this.template_id = template_id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Miniprogram getMiniprogram() {
		return miniprogram;
	}

	public void setMiniprogram(Miniprogram miniprogram) {
		this.miniprogram = miniprogram;
	}

	public Map<String, DataItem> getData() {
		return data;
	}

	public void setData(Map<String, DataItem> data) {
		this.data = data;
	}
	
	public synchronized void addData(String keyName,DataItem dataItem){
		if(this.data == null){
			this.data = new HashMap<String, DataItem>();
		}
		this.data.put(keyName, dataItem);
	}

	@Override
	public String toString() {
		return "sendTemplateMessageParam [touser=" + touser + ", template_id=" + template_id + ", url=" + url
				+ ", miniprogram=" + miniprogram + ", data=" + data + "]";
	}

}