package top.hmtools.wxmp.message.customerService.model.sendMessage;

import top.hmtools.wxmp.message.customerService.model.BaseSendMessageParam;

/**
 * 消息管理--客服消息--发送音乐消息
 * @author HyboWork
 *
 */
public class MusicCustomerServiceMessage extends BaseSendMessageParam{

	private Music music;

	public Music getMusic() {
		return music;
	}

	public void setMusic(Music music) {
		this.music = music;
	}

	@Override
	public String toString() {
		return "MusicCustomerServiceMessage [music=" + music + ", touser=" + touser + ", msgtype=" + msgtype + "]";
	}
	
	
}
