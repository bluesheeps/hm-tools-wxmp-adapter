package top.hmtools.wxmp.material.convert;

import java.util.HashMap;

import org.apache.http.entity.mime.MultipartEntityBuilder;

import top.hmtools.wxmp.core.RequestParamConvert;
import top.hmtools.wxmp.core.httpclient.HmHttpClientTools;

public class UploadMediaRequestParamConvert implements RequestParamConvert {

	@Override
	public MultipartEntityBuilder convert(HashMap<String, Object> params) {
		return HmHttpClientTools.buildMultipartEntity(params);
	}

}
